package com.sda.workshops;

import java.util.Scanner;

/**
 * Created by amen on 03.11.2017.
 */
public class Zadanie5 {
    /**
     * Stworzymy aplikację która wykonuje pewną czynność N razy, gdzie N jest liczbą całkowitą i oznacza ilość powtórzeń
     * podaną przez użytkownika.
     * <p>
     * 1. Na początek wypisz komunikat z pytaniem o ilość powtórzeń.
     * 2. Stwórz obiekt Scanner'a
     * 3. Pobierz od użytkownika ilość powtórzeń i przypisz ją do zmiennej N.
     * 4. N razy wypisz na ekran(konsolę) napis "Hello World!"
     */
    public static void main(String[] args) {

        // wypisanie komunikatu
        System.out.println("Podaj ilość powtórzeń:");
        // stworzenie konsoli
        Scanner konsola = new Scanner(System.in);

        // pobranie ilości powtórzeń:
        int ilośćPowtórzeń = konsola.nextInt();

        // wykonanie czynności ilośćPowtórzeń razy
        for (int licznik = 0; licznik < ilośćPowtórzeń; licznik++) {
            System.out.println("Hello world!");
        }
    }
}
