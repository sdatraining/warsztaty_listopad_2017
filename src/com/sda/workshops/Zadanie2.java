package com.sda.workshops;

/**
 * Created by amen on 03.11.2017.
 */
public class Zadanie2 {
    /*
     * Zadeklaruj 3 zmienne - zmienna 'a', zmienna
     * 'b', zmienna 'c'. Przypisz jej 3 wartości -
     * ważne żeby były różne. Następnie wykonaj na
     * nich następujące działania:
     *
     * a = 5
     * b = 7
     * c = 9
     * a) przepisz wartości:
     *          do zmiennej 'a' przypisz wartość 'b',
     *          do zmiennej 'b' przypisz wartość 'c',
     *          do zmiennej 'c' przypisz wartość 'a'.
     *
     * Oczekiwany wynik wartości zmiennych:
     *       a = 7
     *       b = 9
     *       c = 5
     */
    public static void main(String[] args) {
        // deklaracja 3 zmiennych
        int a = 5;
        int b = 7;
        int c = 9;

        // wypisanie wartości zmiennych na ekran:
        System.out.println("A:" + a + ", B:" + b + ", C:" + c);

        // zmiana wartości zmiennych
        int tmp = a;
        a = b;
        b = c;
        c = tmp;

        // wypisanie wartości na ekran:
        System.out.println("A:" + a + ", B:" + b + ", C:" + c);
    }
}
