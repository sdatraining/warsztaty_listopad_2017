package com.sda.workshops;

import java.util.Scanner;

/**
 * Created by amen on 03.11.2017.
 */
public class Zadanie7 {
    public static void main(String[] args) {
        /*
         * Treścią zadania jest stworzenie pętli która wykona się zadaną przez użytkownika ilość razy. W pętli wykonaj dodawanie liczb
         * z przedziału od 1 do zadanej przez użytkownika liczby. Przykład:
         *
         * Użytkownik podał:
         *          5
         * Działania:
         *          1 + 2 + 3 + 4 + 5
         * Wynik:
         *          15
         *
         * Kroki do wykonania:
         *      1. Zapytaj użytkownika o ilość powtórzeń
         *      2. Pobierz od użytkownika ilość powtórzeń
         *      3. Stwórz zmienną tymczasową (suma)
         *      4. Wykonaj pętle i do sumy dodaj kolejne liczniki w pętli
         *      5. Wypisz na ekran wynik
         */
        // wypisanie komunikatu
        System.out.println("Podaj ilość powtórzeń:");
        // stworzenie konsoli
        Scanner konsola = new Scanner(System.in);

        // pobranie ilości powtórzeń:
        int ilośćPowtórzeń = konsola.nextInt();

        // stworzenie zmiennej przechowującej sumę:
        int suma = 0;

        // wykonanie czynności ilośćPowtórzeń razy
        for (int licznik = 1; licznik <= ilośćPowtórzeń; licznik++) {
            suma = suma + licznik;
        }

        // wypisanie wyniku
        System.out.println("Wynik: " + suma);
    }
}
