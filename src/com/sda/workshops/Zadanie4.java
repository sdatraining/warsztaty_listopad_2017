package com.sda.workshops;

import java.util.Scanner;

/**
 * Created by amen on 03.11.2017.
 */
public class Zadanie4 {
    public static void main(String[] args) {
        /**
         * Zmodyfikuj rozwiązanie zadania 3 i posługując się Scanner'em załaduj z konsoli wejście od użytkownika.
         * Na początku wyświetl użytkownikowi deskryptywny komunikat z pytaniem o wagę, po czym pobierz od użytkownika
         * jego wagę. Potem wyświetl komunikat z pytaniem o wzrost, a następnie pobierz jego wzrost.
         *
         * Pozostałe kroki (weryfikację wzrostu i wagi pozostaw bez zmian).
         *
         * Działanie scannera:
         *  Do pobrania wartości z konsoli konieczne jest stworzenie obiektu zdolnego do czytania wartości z konsoli.
         *  Do tego posłużymy się obiektem Scanner. Jest to klasa napisana (gotowa) w Javie, którą możemy się posłużyć,
         *  aby załadować dowolne wartości z konsoli. Aby pobrać wartość liczbową, możemy użyć na obiekcie Scanner'a
         *  metodę nextInt(). Przykład utworzenia Scannera:
         *
         *  Scanner konsola = new Scanner(System.in);
         *
         *  Przykład użycia Scanner'a do pobrania wartości liczbowej z konsoli:
         *
         *  konsola.nextInt()
         */
        // stworzenie obiektu Scanner'a
        Scanner konsola = new Scanner(System.in);

        // pierwszy komunikat - zwróć uwagę że aplikacja wypisze komunikat, ale zatrzyma się na instrukcji
        // konsola.nextInt() - program będzie czekać, aż użytkownik w konsoli poda wejście.
        System.out.println("Podaj swoją wagę:");
        int waga = konsola.nextInt(); // pobranie wagi

        System.out.println("Podaj swój wzrost:");
        int wzrost = konsola.nextInt();// pobranie wzrostu

        // pozostała część bez zmian:
        if (waga < 180 && wzrost > 150) {
            System.out.println("Możesz wejść na kolejkę.");
        } else {
            System.out.println("Ważysz zbyt dużo lub jesteś zbyt niski/a, nie możesz wejść na kolejkę.");
        }

    }
}
