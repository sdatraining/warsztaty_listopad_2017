package com.sda.workshops;

/**
 * Created by pawel on 03.11.2017.
 */
public class Zadanie3 {
    public static void main(String[] args) {
        /**
         * Aplikacja weryfikująca wagę oraz wzrost osób które mogą wejść na rollercoaster'a.
         *
         * Celem zadania jest zadeklarowanie dwóch zmiennych, którym na początku przypiszemy wartości.
         * Zmienne nazwiemy 'waga' oraz 'wzrost'. Przypisz do nich na początku wartości 80 (waga), oraz 160 (wzrost).
         * Wykorzystując instrukcję warunkową 'if' zweryfikuj poprawną wagę i wzrost osób wchodzących na kolejkę.
         *
         * Każda osoba która waży mniej niż 180 kg, oraz jest wyższa niż 150 cm może wejść na kolejkę.
         *
         * Wykorzystaj operacje logiczne:
         * && - oznacza koniunkcję (and / i)
         * || - oznacza alternatywę (or / lub)
         * > - znak większości
         * < - znak mniejszości
         * >= - znak większe równe
         * <= - znak mniejsze równe
         *
         * Wyświetl odpowiedni komunikat w zależności czy osoba może wejść na kolejkę (warunek jest spełniony), lub
         * nie może wejść na kolejkę (w przeciwnym wypadku - posłuż się instrukcją 'else').
         */
        int waga = 80;
        int wzrost = 160;

        if(waga < 180 && wzrost > 150){
            System.out.println("Ważysz zbyt dużo lub jesteś zbyt niski/a, nie możesz wejść na kolejkę.");
        }else {
            System.out.println("Możesz wejść na kolejkę.");
        }

        if( 10 > 1){
            // instrukcje
        }else if( 5 < 1){
            // inne instrukcje
        }else{
            // w przeciwnym razie te instrukcje
        }
    }
}
