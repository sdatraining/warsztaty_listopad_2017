package com.sda.workshops;

/**
 * Created by amen on 03.11.2017.
 */
public class Zadanie1 {
    public static void main(String[] args) {
        /**
         * Treść:
         * Napisz aplikację w której zadeklaruj dwie zmienne
         * liczbowe, przypisz do nich dwie dowolne wartości.
         * PRZYPISZ DO NICH wartości inicjalne i przemnóż
         * je przez siebie. Wynik działania wypisz na
         * konsoli (System.out.println).
         */
        // deklaracja zmiennej bez inicjalizacji wartości:
        int liczba1;

        // przypisanie wartości (jeden znak równości):
        liczba1 = 5;

        // deklaracja zmiennej oraz inicjalizacja wartości = 10:
        int liczba2 = 10;

        // deklaracja zmiennej 'wynik' oraz wartości mnożenia dwóch zmiennych przez siebie:
        int wynik = liczba1 * liczba2;

        // wypisanie do konsoli: "Wynik: "
        System.out.println("Wynik: ");

        // wypisanie wartości zmiennej 'wynik':
        System.out.println(wynik);

        // wypisanie tego samego co wyżej, tylko w jednej linii:
//        System.out.println("Wynik: " + wynik);
    }
}
