package com.sda.workshops;

import java.util.Scanner;

/**
 * Created by amen on 03.11.2017.
 */
public class Zadanie6 {
    /**
     * Rozwiń zadanie 5. Do każdego wypisania (do każdej wypisanej linii "Hello World!") dopisz numer tego wypisania
     * (numer linii).
     */
    public static void main(String[] args) {
        // wypisanie komunikatu
        System.out.println("Podaj ilość powtórzeń:");
        // stworzenie konsoli
        Scanner konsola = new Scanner(System.in);

        // pobranie ilości powtórzeń:
        int ilośćPowtórzeń = konsola.nextInt();

        // wykonanie czynności ilośćPowtórzeń razy
        for (int licznik = 0; licznik < ilośćPowtórzeń; licznik++) {
            System.out.println((licznik + 1) + ". Hello World");
        }
    }
}
