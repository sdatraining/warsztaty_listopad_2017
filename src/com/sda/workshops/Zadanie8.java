package com.sda.workshops;

import java.util.Scanner;

/**
 * Created by amen on 03.11.2017.
 */
public class Zadanie8 {
    /*
     * Treścią zadania jest stworzenie pętli która wykona się zadaną przez użytkownika ilość razy. W pętli wykonaj dodawanie
     * podanej przez użytkownika ilości liczb. Liczby powinny być ładowane z konsoli. Przed podaniem kolejnej liczby wypisz
     * komunikat o treści "Podaj kolejną liczbę:"
     *
     * Przykład:
     * Użytkownik podał 5 powtórzeń.
     *
     * Wynik działania programu:
     *     Podaj ilość powtórzeń:
     *         5
     *     Podaj kolejną liczbę:
     *         3
     *     Podaj kolejną liczbę:
     *         7
     *     Podaj kolejną liczbę:
     *         2
     *     Podaj kolejną liczbę:
     *         8
     *     Podaj kolejną liczbę:
     *         5
     *
     * Działanie:
     *          3 + 7 + 2 + 8 + 5
     * Wynik:
     *          25
     * Kroki do wykonania:
     *      1. Zapytaj użytkownika o ilość powtórzeń
     *      2. Pobierz od użytkownika ilość powtórzeń
     *      3. Stwórz zmienną tymczasową (suma)
     *      4. Wykonaj pętle i w pętli:
     *         a. wypisz komunikat "Podaj kolejną liczbę:"
     *         b. pobierz kolejną liczbę z konsoli (do zmiennej)
     *         c. dodaj liczbę (wartość zmiennej) do sumy
     *      5. Wypisz na ekran wynik
     */
    public static void main(String[] args) {
        // wypisanie komunikatu
        System.out.println("Podaj ilość powtórzeń:");
        // stworzenie konsoli
        Scanner konsola = new Scanner(System.in);

        // pobranie ilości powtórzeń:
        int ilośćPowtórzeń = konsola.nextInt();

        // stworzenie zmiennej przechowującej sumę:
        int suma = 0;

        // wykonanie czynności ilośćPowtórzeń razy
        for (int licznik = 0; licznik < ilośćPowtórzeń; licznik++) {
            System.out.println("Podaj kolejną liczbę:");
            int kolejnaLiczba = konsola.nextInt();
            suma = suma + kolejnaLiczba;
        }

        // wypisanie wyniku
        System.out.println("Wynik: " + suma);
    }
}
